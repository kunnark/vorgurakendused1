window.onload = function() {
	var allbeads = document.getElementsByClassName("bead");
	for(var i = 0; i < allbeads.length; i++) {
	    // console.log(allbeads[i].style.cssFloat); // -- ei suuda assignida külge style.cssFloat propertyt, seetõttu ei õnnestu otse propertyt muuta
		// kontrollime, kas klass "left" eksisteerib
		var hasClass = allbeads[i].classList.contains('left');
		if(hasClass == true) 
			allbeads[i].style.cssFloat = "right";
		else 
			allbeads[i].style.cssFloat = "left";
	}// for i 
}