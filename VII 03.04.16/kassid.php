<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Praktikum 7 - Kassid</title>
</head>
<body>
<?php
$kassid	= array( 
			array(
					'nimi'=>'Miisu', 
					'vanus'=>2,
					'omanik'=>'Kunnar',
					'pilt'=>'mjau.jpg'
			), 
			array(
				'nimi'=>'Tom', 
				'vanus'=>1,
				'omanik' => 'Ants',
				'pilt' => 'tom.jpg'
			),
			array(
				'nimi'=>'Ints', 
				'vanus'=>8,
				'omanik' => 'Andrus',
				'pilt' => 'ints.jpg'
			),
			array(
				'nimi'=>'Murri', 
				'vanus'=>5,
				'omanik' => 'Heints',
				'pilt' => 'murri.jpg'
			)
	);
	
	foreach($kassid as $kass){
		include("kass_template.html");
	}
?>
</body>
</html>

