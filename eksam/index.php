<?php
/* Kunnar Kukk */
// read like from textfile
$filename = "like.txt";
$fileread = fopen($filename, "r") or die("Ei saanud faili lugeda!");
$likecount = fread($fileread, filesize($filename)); // loeme likede numbri
fclose($fileread);

// Kui postime like nupu, siis suurendame ühe võrra likede arvu
if($_SERVER['REQUEST_METHOD'] == "POST" && array_key_exists("LikeGiven", $_COOKIE) == false){
	$timenumber = 365 * 24 * 60 * 60; // Järgmine like aasta pärast
	setcookie("LikeGiven", "given", time() + $timenumber);  
	$likecount++;
	$myfile = fopen($filename, "w") or die("Ei suutnud faili avada!");
	fwrite($myfile, $likecount);
	fclose($myfile);
}else if($_SERVER['REQUEST_METHOD'] == "POST" && array_key_exists("LikeGiven", $_COOKIE) == true){
	echo "Sul on juba like antud.<br/>";
}// if _SERVER

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Eksam - like nupp | Kunnar Kukk</title>
</head>
<body>
Likede arv: <?php echo $likecount; ?>
<form action="index.php" method="POST" id="likeform">
  <button type="submit" form="likeform" value="Submit">Like</button>
</form>

</body>
</html>
