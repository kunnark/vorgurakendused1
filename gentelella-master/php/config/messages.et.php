<?php
/* Config file for Värbamisbaas */
/* Created 08.04.2016 */

header('Content-Type: application/json; charset=utf-8');

$application[] = array(
	"title" => "Värbamisbaas",
	"author" => "kunnark",
	"designtemplate" => "Gentellela/Colorlib"
);

$side_menu[] = array(
	"candidates" => "Kandidaadid",
	"candidates_all" => "Kõik kandidaadid",
	"candidates_archived" => "Arhiveeritud",
	"actions" => "Tegevused",
	"actions_addperson" => "Lisa isik",
	"actions_roles" => "Rollid",
	"actions_statuses" => "Statuses",
	"actions_steps" => "Etapid"	
);

$page_dashboard[] = array(
	"total_in_database" => "Kokku baasis",
	"active" => "Aktiivsed",
	"archive" => "Arhiivis",
	"from_last_week" => "7 päeva",
	"search" => "Otsi",
	"search_by_name" => "Otsi nime järgi ...",
	"add_new_person" => "Lisa uus",
	"candidates" => "Kandidaadid"
);
$foo = array("1" => "2");

$messages = array(
	"application" => $application,
	"side_menu" => $side_menu,
	"page_dashboard" => $page_dashboard,
	"page_profile" => $foo,
	"page_newPerson" => $foo,
	"page_editPerson" => $foo
);

$json = json_encode($messages, JSON_PRETTY_PRINT);
echo $json;

?>