<?php
/* Roles file for Värbamisbaas */
/* Created 08.04.2016 */
include "../../php/functions.php";

header('Content-Type: application/json; charset=utf-8');
/*
$roles = array(
			array(
					"url" => "java-arendaja",
					"name" => "Java arendaja",
					"ID" => 0
			),
			array(
					"url" => "net-arendaja",
					"name" => ".NET arendaja",
					"ID" => 1
			),
			array(
					"url" => "scrum-master",
					"name" => "Scrum master",
					"ID" => 2
			)
);
*/

$roles = getRolesAsListDashboard();
$json = json_encode($roles, JSON_PRETTY_PRINT);
echo $json;

?>