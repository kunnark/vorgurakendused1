<?php
$table_persons = "kukukk_persons";
$table_roles = "kukukk_roles";
$table_files = "kukukk_failid";
$table_comments = "kukukk_comments";

function connect_db(){
	global $connection;
	$host="localhost";
	$user="test";
	$pass="t3st3r123";
	$db="test";
	$connection = mysqli_connect($host, $user, $pass, $db) or die("ei saa ühendust mootoriga- ".mysqli_error());
	mysqli_query($connection, "SET CHARACTER SET UTF8") or die("Ei saanud baasi utf-8-sse - ".mysqli_error($connection));
}

function getProfile($id){
	global $table_persons;
	connect_db();
		$query = "SELECT * FROM $table_persons WHERE ID = $id limit 1";
		$result = mysqli_query($GLOBALS['connection'], $query) or die("$query - ".mysqli_error($GLOBALS['connection']));	
		$row = mysqli_fetch_assoc($result);
	return $row;
}//getProfile($id)


function getRoles($personID){
	global $table_persons;
	connect_db();
	
		$query = "SELECT roles FROM $table_persons WHERE ID = $personID";
		$result = mysqli_query($GLOBALS['connection'], $query) or die("$query - ".mysqli_error($GLOBALS['connection']));	
		$row = mysqli_fetch_assoc($result);

	$roles = getPersonRolesList($row["roles"]);
	return $roles;
}// getRoles

function getPersonRolesList($commaseparatedRoles){
	$roles = explode(",", $commaseparatedRoles);
	$result = array();
	foreach($roles as $roleID){
		$role = array();
			$role = getPersonRole($roleID);
			$result[] = $role;
	}
	return $result;
}// getRoles

function getPersonRole($roleID){
	global $table_roles;
	connect_db();
		$query = "SELECT * FROM $table_roles WHERE ID = $roleID";
		$result = mysqli_query($GLOBALS['connection'], $query) or die("$query - ".mysqli_error($GLOBALS['connection']));	
		$row = mysqli_fetch_assoc($result);
	return $row;
}//getProfile($id)

// Rollide listi jaoks
function getRolesAsListDashboard(){
	global $table_roles;
	connect_db();
		$query = "SELECT * FROM $table_roles";
		$result = mysqli_query($GLOBALS['connection'], $query) or die("$query - ".mysqli_error($GLOBALS['connection']));	

		$role = array();
		$roles = array();	
			
		while ($row = mysqli_fetch_row($result)){
			
		    $role["url"] = $row[3];
			$role["name"] = $row[1];
		    $role["ID"] = $row[0];
			$roles[] = $role;
		}// while
	
	return $roles;
}// getRolesAsListDashboard

// Pooleli
function getStatisticsOnDashboard(){
	global $table_persons;
	connect_db();
	$statistics = array();
	// Kõik kokku
		$query = "SELECT count(ID) FROM $table_persons";
		$result = mysqli_query($GLOBALS['connection'], $query) or die("$query - ".mysqli_error($GLOBALS['connection']));	
		$row = mysqli_fetch_row($result);
		
		$statistics["totalcount"] = $row[0];
		
		$query = "SELECT count(ID) FROM $table_persons WHERE status = 'active'";
		$result = mysqli_query($GLOBALS['connection'], $query) or die("$query - ".mysqli_error($GLOBALS['connection']));	
		$row = mysqli_fetch_row($result);
		
		$statistics["activepersons"] = $row[0];
		
		$query = "SELECT count(ID) FROM $table_persons WHERE status = 'archived'";
		$result = mysqli_query($GLOBALS['connection'], $query) or die("$query - ".mysqli_error($GLOBALS['connection']));	
		$row = mysqli_fetch_row($result);
		
		$statistics["archivedpersons"] = $row[0]; 
	return $statistics;
}

function getPersonsAsListDashboard(){
	global $table_persons;
	connect_db();
		$query = "SELECT ID, eesnimi, perenimi, viimane_kontakt, hinne, roles 
					FROM $table_persons where status = 'active' 
					ORDER BY viimane_kontakt DESC";
		$result = mysqli_query($GLOBALS['connection'], $query) or die("$query - ".mysqli_error($GLOBALS['connection']));	
		
		$person = array();
		$persons = array();
		
		while($row = mysqli_fetch_row($result)){
			$person["ID"] = $row[0];
			$person["eesnimi"] = $row[1];
			$person["perenimi"] = $row[2];
			$person["viimane_kontakt"] = $row[3];
			$person["hinne"] = $row[4];
			$person["roles"] = $row[5];
			$persons[] = $person;
		}// while	
	return $persons;	
}// getPersonsAsListDashboard()


function getFiles($personID){
	global $table_files;
	connect_db();
		$query = "SELECT * FROM $table_files WHERE personID = $personID";
		$result = mysqli_query($GLOBALS['connection'], $query) or die("$query - ".mysqli_error($GLOBALS['connection']));	
		$row = mysqli_fetch_assoc($result);
	return $row;
}//getProfile($id)

// **** Kommentaarid *** 
function getComments($personID){
	global $table_comments;
	connect_db();
		$query = "SELECT * FROM $table_comments WHERE personID = $personID ORDER BY datetime ASC";
		$result = mysqli_query($GLOBALS['connection'], $query) or die("$query - ".mysqli_error($GLOBALS['connection']));	
		$comments = array();
		while ($row = mysqli_fetch_row($result)){
		    $comments[] = $row;
		}// while
	return $comments;
}//getProfile($id)

function addComment($personID, $comment){
	global $table_comments;
	connect_db();
	$datetime = date("Y-m-d H:i:s");
	$comment = htmlspecialchars($comment);
		$query = "INSERT INTO $table_comments (personID, content, datetime) VALUES($personID, '$comment', '$datetime')";
		$result = mysqli_query($GLOBALS['connection'], $query) or die("$query - ".mysqli_error($GLOBALS['connection']));
}// addComment

// **** Kommentaarid *** 

// ** Add new person
// $personData - array
function addNewPerson($personData){
	global $table_comments, $table_persons, $connection;
	connect_db();

	$eesnimi = $personData["eesnimi"];
	$perenimi = $personData["perenimi"];
	$email = $personData["email"];
	$viimane_kontakt = $personData["viimane_kontakt"];
	$mobiil = $personData["mobiil"];
	$cv_url = $personData["cv_url"];
	$labitud_etapid = $personData["labitud_etapid"];
	$toopakkumine = $personData["toopakkumine"];
	$hinne = $personData["hinne"];
	$roles = $personData["roles"];
	$comment = $personData["comment"];
	
	$query = "INSERT INTO $table_persons (eesnimi, perenimi, email, viimane_kontakt, mobiil, cv_url, labitud_etapid, toopakkumine, hinne, roles, status) VALUES('$eesnimi','$perenimi','$email','$viimane_kontakt','$mobiil','$cv_url','$labitud_etapid','$toopakkumine','$hinne','$roles','active')";
	$result = mysqli_query($GLOBALS['connection'], $query) or die("$query - ".mysqli_error($GLOBALS['connection']));
	
	$id = mysqli_insert_id($connection);
	header("Location: profile.php?id=$id");
}// addNewPerson

?>