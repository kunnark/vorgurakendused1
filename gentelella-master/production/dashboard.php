<?php 
include("../php/functions.php");
require_once("../templates/header.php");

$statistics = getStatisticsOnDashboard();
$persons = getPersonsAsListDashboard();

?>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">

					<div class="navbar nav_title" style="border: 0;">
						<a href="index.html" class="site_title"><i class="fa fa-home"></i> <span id="application_title">Värbamisbaas</span></a>
					</div>
					<div class="clearfix"></div>

					<?php require_once("../templates/sidebar_menu.php");  ?>
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">
				
				<div class="nav_menu">
					<nav class="" role="navigation">
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>
					</nav>
				</div>
				
				

			</div>
			<!-- /top navigation -->

			<!-- page content -->
			<div class="right_col" role="main">
				<?php 
				// Kuvame statistika
					require_once("../templates/dashboardstatistics.php"); 
				?>
				
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="dashboard_graph">

							<div class="row x_title">
								<div class="col-md-6">
									<h3 id="candidates"></h3>
								</div>
								<div class="col-md-2 col-sm-2 col-md-offset-4 col-sm-offset-4">
									<a href="addPerson.php"><button class="btn btn-success" type="button" id="add_new_person"><i class="fa fa-user"></i></button></a>
								</div>
								
							</div>
							<!-- Dashboard contents-->
							<div class="col-md-12 col-sm-12 col-xs-12">
								
									<div class="col-md-12">
									              <div class="x_panel">
									                <div class="x_content">

									                  <div class="row">

									                    <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;" id="roles">
									                      
									                    </div>
									                    <div class="clearfix"></div>

															<?php 
															// Fetchime isikud kaartidena
															require_once("../templates/dashboardpersons.php"); 
															?>
						
																		
											</div>
										</div>
									</div>
							
							</div>
							</div>
							<!--// Dashboard contents -->
							<div class="clearfix"></div>
						</div>
					</div>

				</div>
				<br />

				<!-- footer content -->

				
				<!-- /footer content -->
			</div>
			<!-- /page content -->

		</div>

	</div>

	</div>

		<script src="js/bootstrap.min.js"></script>

		<!-- gauge js -->
		<script type="text/javascript" src="js/gauge/gauge.min.js"></script>
		<script type="text/javascript" src="js/gauge/gauge_demo.js"></script>
		<!-- chart js -->
		<script src="js/chartjs/chart.min.js"></script>
		<!-- bootstrap progress js -->
		<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
		<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
		<!-- icheck -->
		<script src="js/icheck/icheck.min.js"></script>
		<!-- daterangepicker -->
		<script type="text/javascript" src="js/moment/moment.min.js"></script>
		<script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>

		<script src="js/custom.js"></script>

		<!-- flot js -->
		<!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
		<script type="text/javascript" src="js/flot/jquery.flot.js"></script>
		<script type="text/javascript" src="js/flot/jquery.flot.pie.js"></script>
		<script type="text/javascript" src="js/flot/jquery.flot.orderBars.js"></script>
		<script type="text/javascript" src="js/flot/jquery.flot.time.min.js"></script>
		<script type="text/javascript" src="js/flot/date.js"></script>
		<script type="text/javascript" src="js/flot/jquery.flot.spline.js"></script>
		<script type="text/javascript" src="js/flot/jquery.flot.stack.js"></script>
		<script type="text/javascript" src="js/flot/curvedLines.js"></script>
		<script type="text/javascript" src="js/flot/jquery.flot.resize.js"></script>
		<script>
			$(document).ready(function() {
				// [17, 74, 6, 39, 20, 85, 7]
				//[82, 23, 66, 9, 99, 6, 2]
				var data1 = [
					[gd(2012, 1, 1), 17],
					[gd(2012, 1, 2), 74],
					[gd(2012, 1, 3), 6],
					[gd(2012, 1, 4), 39],
					[gd(2012, 1, 5), 20],
					[gd(2012, 1, 6), 85],
					[gd(2012, 1, 7), 7]
				];

				var data2 = [
					[gd(2012, 1, 1), 82],
					[gd(2012, 1, 2), 23],
					[gd(2012, 1, 3), 66],
					[gd(2012, 1, 4), 9],
					[gd(2012, 1, 5), 119],
					[gd(2012, 1, 6), 6],
					[gd(2012, 1, 7), 9]
				];
				$("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
					data1, data2
				], {
					series: {
						lines: {
							show: false,
							fill: true
						},
						splines: {
							show: true,
							tension: 0.4,
							lineWidth: 1,
							fill: 0.4
						},
						points: {
							radius: 0,
							show: true
						},
						shadowSize: 2
					},
					grid: {
						verticalLines: true,
						hoverable: true,
						clickable: true,
						tickColor: "#d5d5d5",
						borderWidth: 1,
						color: '#fff'
					},
					colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
					xaxis: {
						tickColor: "rgba(51, 51, 51, 0.06)",
						mode: "time",
						tickSize: [1, "day"],
						//tickLength: 10,
						axisLabel: "Date",
						axisLabelUseCanvas: true,
						axisLabelFontSizePixels: 12,
						axisLabelFontFamily: 'Verdana, Arial',
						axisLabelPadding: 10
							//mode: "time", timeformat: "%m/%d/%y", minTickSize: [1, "day"]
					},
					yaxis: {
						ticks: 8,
						tickColor: "rgba(51, 51, 51, 0.06)",
					},
					tooltip: false
				});

				function gd(year, month, day) {
					return new Date(year, month - 1, day).getTime();
				}
			});
		</script>

		<!-- worldmap -->
		<script type="text/javascript" src="js/maps/jquery-jvectormap-2.0.3.min.js"></script>
		<script type="text/javascript" src="js/maps/gdp-data.js"></script>
		<script type="text/javascript" src="js/maps/jquery-jvectormap-world-mill-en.js"></script>
		<script type="text/javascript" src="js/maps/jquery-jvectormap-us-aea-en.js"></script>
		<!-- pace -->
		<script src="js/pace/pace.min.js"></script>
		
		<script type="text/javascript" src="js/autocomplete/countries.js"></script>
		<script>
			$(function() {
				$('#world-map-gdp').vectorMap({
					map: 'world_mill_en',
					backgroundColor: 'transparent',
					zoomOnScroll: false,
					series: {
						regions: [{
							values: gdpData,
							scale: ['#E6F2F0', '#149B7E'],
							normalizeFunction: 'polynomial'
						}]
					},
					onRegionTipShow: function(e, el, code) {
						el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
					}
				});
			});
		</script>
		<!-- skycons -->
		<script src="js/skycons/skycons.min.js"></script>
		<script>
			var icons = new Skycons({
					"color": "#73879C"
				}),
				list = [
					"clear-day", "clear-night", "partly-cloudy-day",
					"partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
					"fog"
				],
				i;

			for (i = list.length; i--;)
				icons.set(list[i], list[i]);

			icons.play();
		</script>

		<!-- dashbord linegraph -->
		<script>
			var doughnutData = [{
				value: 30,
				color: "#455C73"
			}, {
				value: 30,
				color: "#9B59B6"
			}, {
				value: 60,
				color: "#BDC3C7"
			}, {
				value: 100,
				color: "#26B99A"
			}, {
				value: 120,
				color: "#3498DB"
			}];
			var myDoughnut = new Chart(document.getElementById("canvas1").getContext("2d")).Doughnut(doughnutData);
		</script>
		<!-- /dashbord linegraph -->
		
		<script>
			NProgress.done();
		</script>
	
	
	
	
	
	
	
	<script>
	
		$(document).ready(function() {
			// [17, 74, 6, 39, 20, 85, 7]
			//[82, 23, 66, 9, 99, 6, 2]
			var data1 = [
				[gd(2012, 1, 1), 17],
				[gd(2012, 1, 2), 74],
				[gd(2012, 1, 3), 6],
				[gd(2012, 1, 4), 39],
				[gd(2012, 1, 5), 20],
				[gd(2012, 1, 6), 85],
				[gd(2012, 1, 7), 7]
			];

			var data2 = [
				[gd(2012, 1, 1), 82],
				[gd(2012, 1, 2), 23],
				[gd(2012, 1, 3), 66],
				[gd(2012, 1, 4), 9],
				[gd(2012, 1, 5), 119],
				[gd(2012, 1, 6), 6],
				[gd(2012, 1, 7), 9]
			];
			$("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
				data1, data2
			], {
				series: {
					lines: {
						show: false,
						fill: true
					},
					splines: {
						show: true,
						tension: 0.4,
						lineWidth: 1,
						fill: 0.4
					},
					points: {
						radius: 0,
						show: true
					},
					shadowSize: 2
				},
				grid: {
					verticalLines: true,
					hoverable: true,
					clickable: true,
					tickColor: "#d5d5d5",
					borderWidth: 1,
					color: '#fff'
				},
				colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
				xaxis: {
					tickColor: "rgba(51, 51, 51, 0.06)",
					mode: "time",
					tickSize: [1, "day"],
					//tickLength: 10,
					axisLabel: "Date",
					axisLabelUseCanvas: true,
					axisLabelFontSizePixels: 12,
					axisLabelFontFamily: 'Verdana, Arial',
					axisLabelPadding: 10
						//mode: "time", timeformat: "%m/%d/%y", minTickSize: [1, "day"]
				},
				yaxis: {
					ticks: 8,
					tickColor: "rgba(51, 51, 51, 0.06)",
				},
				tooltip: false
			});

			function gd(year, month, day) {
				return new Date(year, month - 1, day).getTime();
			}
			
			// Added for messages conf:
			$.get( "../php/config/messages.et.php", function( messages ) {
				$("#add_new_person").text(messages.page_dashboard[0].add_new_person);
				$("#active").html(messages.page_dashboard[0].active);
				$("#archive").html(messages.page_dashboard[0].archive);
				$("[id=from_last_week]").html(messages.page_dashboard[0].from_last_week);
				$("#search").html(messages.page_dashboard[0].search);
				$("#search_by_name").attr("placeholder", messages.page_dashboard[0].search_by_name);
				$("#candidates").html(messages.page_dashboard[0].candidates);
				$("#total_in_database").html(messages.page_dashboard[0].total_in_database);		
			});
			
			// Added for roles:
			var global_roles = {};
			
			    $.ajax({
			    	url: "../php/data/roles.php",
			    	async: false,
			    	dataType: 'json',
			    	success: function(roles) {
						global_roles = roles;
						var items = [];
						
						// väljund
						items.push('<ul class="list-unstyled">');	// pagination pagination-split  
						
						// Nupp kõikide rollide näitamiseks 
						items.push('<li class="role-id inline" id="show-all-roles"><button class="btn btn-round btn-warning">Näita kõiki</button></li>');                  	             
							$.each(roles, function(index, value ){
								items.push('<li class="role-id inline" id="'+roles[index].url+'"><button class="btn btn-round btn-warning">'+roles[index].name+'</button></li>');
							});
						items.push('</ul>');
				 	   $('#roles').html(items.join(''));
			    	}// success
			    });
								
					$(document).on('click', '[class^=role-id]', function(){
						var role_id = $(this).attr("id");
						
						// Näita kõiki:
						if(role_id == "show-all-roles"){
							$('*[id*=person-]').show(); 
						}else{
							// Näita konkreetset gruppi:
							var person_id = 'person-'+role_id; // loome id=person-java-arendaja, mis vastab ühelel isikudivile
							$('*[id*=person-]').hide(); // Lükkame välja korraks puhtaks
							$('[id='+person_id+']').show(); // näitame sobivat rolli	
						}// if role_id
					});
					
				
		});
	</script>

</body>

</html>
