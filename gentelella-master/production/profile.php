<?php
include "../php/functions.php";

if($_SERVER['REQUEST_METHOD'] == "GET"){
	if(is_numeric($_GET["id"])){
		$id = $_GET["id"];
		$person = getProfile($id); 
		$roles = getRoles($id);
		$comments = getComments($id);
	}// if isset
}// if get

if($_SERVER['REQUEST_METHOD'] == "POST"){
	// Teeme POSTI
	$id = $_POST["ID"];
	addComment($id, $_POST["comment"]);
	header("Location: profile.php?id=$id");
}// 
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Värbamine</title>

	<!-- Bootstrap core CSS -->

	<link href="css/bootstrap.min.css" rel="stylesheet">

	<link href="fonts/css/font-awesome.min.css" rel="stylesheet">
	<link href="css/animate.min.css" rel="stylesheet">

	<!-- Custom styling plus plugins -->
	<link href="css/custom.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.3.css" />
	<link href="css/icheck/flat/green.css" rel="stylesheet" />
	<link href="css/floatexamples.css" rel="stylesheet" type="text/css" />
	
	<script src="js/jquery.min.js"></script>
	<script src="js/nprogress.js"></script>
	
	<!-- Added -->
	<script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>
	<!--[if lt IE 9]>
	<script src="../assets/js/ie8-responsive-file-warning.js"></script>
	<![endif]-->

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>


<body class="nav-md">

	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">

					<div class="navbar nav_title" style="border: 0;">
						<a href="index.html" class="site_title"><i class="fa fa-home"></i> <span>Värbamisbaas</span></a>
					</div>
					<div class="clearfix"></div>

					<!-- sidebar menu -->
						<?php require_once("../templates/sidebar_menu.php");  ?>
					<!-- /sidebar menu -->

					<!-- /menu footer buttons -->
					<div class="sidebar-footer hidden-small">
						<a data-toggle="tooltip" data-placement="top" title="Settings">
							<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="FullScreen">
							<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Lock">
							<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Logout">
							<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
						</a>
					</div>
					<!-- /menu footer buttons -->
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">

				<div class="nav_menu">
					<nav class="" role="navigation">
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>
					</nav>
				</div>

			</div>
			<!-- /top navigation -->


			<!-- page content -->
			<div class="right_col" role="main">

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						
								<div class="col-md-6">
									<h3>Profiil</h3>
								</div>
								
						

							
							
							<!-- Profiilisisu -->
							<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="x_panel">
									          
									                <div class="x_content">

									                  <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
									                  
														 
									                    <h3><?php 
																echo $person["eesnimi"];
																echo " "; 
																echo $person["perenimi"];
															?>
														</h3>

									                    <ul class="list-unstyled user_data">
								                      		
															
									                      <li>
									                        <i class="fa fa-briefcase user-profile-icon"></i> 
															<?php 
																foreach($roles as $role){
																	echo $role["name"];
																	echo " ";
																}
															?>
									                      </li>
							                      			<li>
							                       	 			<i class="fa fa-envelope-o"></i> <?php echo $person["email"]; ?>
							                      			</li>
							                      			<li>
							                       	 			<i class="fa fa-phone"></i> <?php echo $person["mobiil"]; ?>
							                      			</li>
									                      <li class="m-top-xs">
									                        <i class="fa fa-external-link user-profile-icon"></i>
									                        <a href="<?php echo $person["cv_url"]; ?>" target="_blank"><?php echo $person["cv_url"]; ?></a>
									                      </li>
														  
														  	<li>
																	<i class="fa fa-file"></i> CV
															</li>
							                      			
									                    </ul>

									                    

													 </div>
													 <!-- Keskmine veerg -->
													 <div class="col-md-3 col-sm-3 col-xs-12" style="margin-top:45px">
 									                    <ul class="list-unstyled user_data">
														 
								                      		
								                      		<li>
								                       	 		<i class="fa fa-calendar-check-o"></i> <?php echo $person["viimane_kontakt"]; ?> (viimane kontakt)
								                      		</li>
								   
															<li>
								                       	 		<i class="fa fa-check-circle-o green"></i> <?php echo $person["labitud_etapid"]; ?>
								                      		</li>
															<li>
								                       	 		<i class="fa fa-check-circle-o green"></i> Testülesanne <i class="fa fa-star"></i> <?php echo $person["hinne"]; ?>
								                      		</li>
															
														</ul>
									                 	
													 </div>
													 <!-- Parem veerg -->
													 <div class="col-md-3 col-sm-3 col-xs-12 profile_right" style="margin-top:45px">
 									                    <ul class="list-unstyled user_data">
														 
								                      
															<li>
								                       	 		<i class="fa fa-balance-scale"></i> <?php echo $person["toopakkumine"]; ?>
								                      		</li>
															
														</ul>
									                    <a class="btn btn-warning"><i class="fa fa-edit m-right-xs"></i> Edit Profile</a>
									                    <br>
													 </div>
													 
												 </div>
										</div>
							</div>
							<!--// Profiilisisu -->
							<!-- Kommentaar -->
							
							
								 <form role="form" action="profile.php" method="POST" id="commentform">
										 <div class="col-md-12 col-sm-12 col-xs-12 form-group">
										
								  	   		 <label for="comment">Kommentaar:</label>
								  	  			<textarea class="form-control" rows="5" id="comment" name="comment" form="commentform"></textarea>
												<input type="hidden" name="ID" value="<?php echo $person["ID"]; ?>">
					
											</div>
										<div class="col-md-9 col-sm-9 form-group">
											<button class="btn btn-success" type="submit"><i class="fa fa-commenting-o"></i> Lisa kommentaar</button>
										</div>
								</form>
							
							<!-- // Kommentaar -->
						</div>
						<!-- Kommentaarid -->
						
						<div class="col-md-12 col-sm-12 col-xs-12">	
							
							<?php 
							
								foreach($comments as $comment){
									$content = $comment[2];
									$date = $comment[3];
										echo "<div class=\"x_panel\">						          
										   	 	<div class=\"x_content\">
										";
										
										echo "<p><strong>Kasutaja</strong> | <i class=\"fa fa-calendar\"></i>	$date</p>";
										echo "<p>$content</p>";
										echo "</div>
											</div>
										";	
									
								}// foreach
							
							?>
							
							
							<div class="x_panel">						          
							   <div class="x_content">
									<p><strong>Kasutaja</strong> | <i class="fa fa-calendar"></i> 04.04.2016 10:45:12</p>
									<p>Lorem ipsum </p>
								</div>
							</div>
						</div>
						
						
						<!-- // Kommentaarid -->
				
				</div>
			</div>
			<!-- /page content -->

		</div>

	</div>

	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>

	<script src="js/bootstrap.min.js"></script>

	<!-- gauge js -->
	<script type="text/javascript" src="js/gauge/gauge.min.js"></script>
	<script type="text/javascript" src="js/gauge/gauge_demo.js"></script>
	<!-- chart js -->
	<script src="js/chartjs/chart.min.js"></script>
	<!-- bootstrap progress js -->
	<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
	<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script src="js/icheck/icheck.min.js"></script>
	<!-- daterangepicker -->
	<script type="text/javascript" src="js/moment/moment.min.js"></script>
	<script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>

	<script src="js/custom.js"></script>

	<!-- flot js -->
	<!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
	<script type="text/javascript" src="js/flot/jquery.flot.js"></script>
	<script type="text/javascript" src="js/flot/jquery.flot.pie.js"></script>
	<script type="text/javascript" src="js/flot/jquery.flot.orderBars.js"></script>
	<script type="text/javascript" src="js/flot/jquery.flot.time.min.js"></script>
	<script type="text/javascript" src="js/flot/date.js"></script>
	<script type="text/javascript" src="js/flot/jquery.flot.spline.js"></script>
	<script type="text/javascript" src="js/flot/jquery.flot.stack.js"></script>
	<script type="text/javascript" src="js/flot/curvedLines.js"></script>
	<script type="text/javascript" src="js/flot/jquery.flot.resize.js"></script>
	<script>
		$(document).ready(function() {
			// [17, 74, 6, 39, 20, 85, 7]
			//[82, 23, 66, 9, 99, 6, 2]
			var data1 = [
				[gd(2012, 1, 1), 17],
				[gd(2012, 1, 2), 74],
				[gd(2012, 1, 3), 6],
				[gd(2012, 1, 4), 39],
				[gd(2012, 1, 5), 20],
				[gd(2012, 1, 6), 85],
				[gd(2012, 1, 7), 7]
			];

			var data2 = [
				[gd(2012, 1, 1), 82],
				[gd(2012, 1, 2), 23],
				[gd(2012, 1, 3), 66],
				[gd(2012, 1, 4), 9],
				[gd(2012, 1, 5), 119],
				[gd(2012, 1, 6), 6],
				[gd(2012, 1, 7), 9]
			];
			$("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
				data1, data2
			], {
				series: {
					lines: {
						show: false,
						fill: true
					},
					splines: {
						show: true,
						tension: 0.4,
						lineWidth: 1,
						fill: 0.4
					},
					points: {
						radius: 0,
						show: true
					},
					shadowSize: 2
				},
				grid: {
					verticalLines: true,
					hoverable: true,
					clickable: true,
					tickColor: "#d5d5d5",
					borderWidth: 1,
					color: '#fff'
				},
				colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
				xaxis: {
					tickColor: "rgba(51, 51, 51, 0.06)",
					mode: "time",
					tickSize: [1, "day"],
					//tickLength: 10,
					axisLabel: "Date",
					axisLabelUseCanvas: true,
					axisLabelFontSizePixels: 12,
					axisLabelFontFamily: 'Verdana, Arial',
					axisLabelPadding: 10
						//mode: "time", timeformat: "%m/%d/%y", minTickSize: [1, "day"]
				},
				yaxis: {
					ticks: 8,
					tickColor: "rgba(51, 51, 51, 0.06)",
				},
				tooltip: false
			});

			function gd(year, month, day) {
				return new Date(year, month - 1, day).getTime();
			}
			
			
		});
	</script>

	<!-- worldmap -->
	<script type="text/javascript" src="js/maps/jquery-jvectormap-2.0.3.min.js"></script>
	<script type="text/javascript" src="js/maps/gdp-data.js"></script>
	<script type="text/javascript" src="js/maps/jquery-jvectormap-world-mill-en.js"></script>
	<script type="text/javascript" src="js/maps/jquery-jvectormap-us-aea-en.js"></script>
	<!-- pace -->
	<script src="js/pace/pace.min.js"></script>
	<script>
		$(function() {
			$('#world-map-gdp').vectorMap({
				map: 'world_mill_en',
				backgroundColor: 'transparent',
				zoomOnScroll: false,
				series: {
					regions: [{
						values: gdpData,
						scale: ['#E6F2F0', '#149B7E'],
						normalizeFunction: 'polynomial'
					}]
				},
				onRegionTipShow: function(e, el, code) {
					el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
				}
			});
		});
	</script>
	<!-- skycons -->
	<script src="js/skycons/skycons.min.js"></script>
	<script>
		var icons = new Skycons({
				"color": "#73879C"
			}),
			list = [
				"clear-day", "clear-night", "partly-cloudy-day",
				"partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
				"fog"
			],
			i;

		for (i = list.length; i--;)
			icons.set(list[i], list[i]);

		icons.play();
	</script>

	<!-- dashbord linegraph -->
	<script>
		var doughnutData = [{
			value: 30,
			color: "#455C73"
		}, {
			value: 30,
			color: "#9B59B6"
		}, {
			value: 60,
			color: "#BDC3C7"
		}, {
			value: 100,
			color: "#26B99A"
		}, {
			value: 120,
			color: "#3498DB"
		}];
		var myDoughnut = new Chart(document.getElementById("canvas1").getContext("2d")).Doughnut(doughnutData);
	</script>
	<!-- /dashbord linegraph -->
	<!-- datepicker -->
	<script type="text/javascript">
		$(document).ready(function() {
			
			var cb = function(start, end, label) {
				console.log(start.toISOString(), end.toISOString(), label);
				$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
				//alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
			}

			var optionSet1 = {
				startDate: moment().subtract(29, 'days'),
				endDate: moment(),
				minDate: '01/01/2012',
				maxDate: '12/31/2015',
				dateLimit: {
					days: 60
				},
				showDropdowns: true,
				showWeekNumbers: true,
				timePicker: false,
				timePickerIncrement: 1,
				timePicker12Hour: true,
				ranges: {
					'Today': [moment(), moment()],
					'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					'This Month': [moment().startOf('month'), moment().endOf('month')],
					'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				},
				opens: 'left',
				buttonClasses: ['btn btn-default'],
				applyClass: 'btn-small btn-primary',
				cancelClass: 'btn-small',
				format: 'MM/DD/YYYY',
				separator: ' to ',
				locale: {
					applyLabel: 'Submit',
					cancelLabel: 'Clear',
					fromLabel: 'From',
					toLabel: 'To',
					customRangeLabel: 'Custom',
					daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
					monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
					firstDay: 1
				}
			};
			$('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
			$('#reportrange').daterangepicker(optionSet1, cb);
			$('#reportrange').on('show.daterangepicker', function() {
				console.log("show event fired");
			});
			$('#reportrange').on('hide.daterangepicker', function() {
				console.log("hide event fired");
			});
			$('#reportrange').on('apply.daterangepicker', function(ev, picker) {
				console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
			});
			$('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
				console.log("cancel event fired");
			});
			$('#options1').click(function() {
				$('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
			});
			$('#options2').click(function() {
				$('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
			});
			$('#destroy').click(function() {
				$('#reportrange').data('daterangepicker').remove();
			});
		});
	</script>
	<script>
		NProgress.done();
	</script>
	<!-- /datepicker -->
	
	<!-- Datepicker added -->
	<script type="text/javascript">
	    $(document).ready(function() {
	      
	      $('#single_cal2').daterangepicker({
	        singleDatePicker: true,
	        calender_style: "picker_2"
	      }, function(start, end, label) {
	        console.log(start.toISOString(), end.toISOString(), label);
	      });
	      
		  
		   $(":input").inputmask();
	    });
	  </script>
	
	<!-- /footer content -->
	

</body>

</html>
