													<?php 
													
													foreach ($persons as $person){
														$role = getRoles($person["ID"]);
														// eeldame, et praegu on 1 roll küljes igal isikul
														echo "
									                   	 	<div class=\"col-md-4 col-sm-4 col-xs-12\" id=\"person-".$role[0]["short_url"]."\">
									                    		<div class=\"well profile_view\">
									                       	 		<div class=\"col-sm-12\">
									                         	   		<h4 class=\"brief\">
																			<i>".$role[0]["name"]."</i>
															 			 </h4>
									                          		   		<div class=\"left col-xs-7\">
									                           			 		<h2>".$person["eesnimi"]."<br>".$person["perenimi"]."</h2>
									                            				<p><strong>Viimane kontakt: </strong> ".$person["viimane_kontakt"]."</p>
																				<p>&nbsp;</p>
																				<p>&nbsp;</p>
									                         				 </div>
									                          			  	<div class=\"right col-xs-5 text-center\">
									                           			 		<img src=\"images/user.png\" alt=\"\" class=\"img-circle img-responsive\">
									                          				 </div>
									                       			</div>
									                        		<div class=\"col-xs-12 bottom text-center\">
									                        			<div class=\"col-xs-12 col-sm-6 emphasis\">
									                         		   		<h2><span class=\"fa fa-star-o\">&nbsp;</span>".$person["hinne"]."</h2>
									                          			 </div>
									                          		   	<div class=\"col-xs-12 col-sm-6 emphasis\">
									                            				<a href=\"profile.php?id=".$person["ID"]."\"><button type=\"button\" class=\"btn btn-primary btn-xs\"> <i class=\"fa fa-user\">
									                                                 </i> Vaata profiili</button>
																				</a>
									                          			</div>
									                       			 </div>
									                      		   </div>
									                    		</div>
															";
													}// foreach persons
														
													?>