				<!--  Statistika -->
				
				<div class="row tile_count">
						
						
						<div class="animated flipInY col-md-2 col-sm-2 col-xs-4 tile_stats_count">
				            <div class="left"></div>
				            <div class="right">
				              <span class="count_top"><i class="fa fa-user"></i></span>
							   <span class="count_top" id="total_in_database"></span>
				             	 <div class="count"><center><?php echo $statistics["totalcount"]; ?></center></div>
				            </div>
						</div>
						
						<div class="animated flipInY col-md-2 col-sm-2 col-xs-4 tile_stats_count">
				            <div class="left"></div>
				            <div class="right">
				              <span class="count_top"><i class="fa fa-flash"></i></span>
							  	<span class="count_top" id="active"></span>
				              <div class="count"><center><?php echo $statistics["activepersons"]; ?></center></div>
				            </div>
						</div>
						
						<div class="animated flipInY col-md-2 col-sm-2 col-xs-4 tile_stats_count">
				            <div class="left"></div>
				            <div class="right">
				              <span class="count_top"><i class="fa fa-archive"></i></span>
							  	<span class="count_top" id="archive"></span>
				              <div class="count"><center><?php echo $statistics["archivedpersons"]; ?></center></div>
				            </div>
						</div>
						
						<div class="col-md-6 col-sm-6 col-xs-4">
				           
				            
									<div class="input-group">													  
													  <input type="text" name="search" id="autocomplete-custom-append" class="form-control col-md-10" style="float: left;" autocomplete="off" id="search_by_name" placeholder="">
									                  <span class="input-group-btn">
									                            <button class="btn btn-default" type="button" id="search"></button>
									                        </span>
									 </div>
				         
						</div>
				
				<!-- // Statistika -->