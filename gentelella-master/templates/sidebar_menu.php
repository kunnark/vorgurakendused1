					<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

						<div class="menu_section">
							<ul class="nav side-menu">
								<li><a><i class="fa fa-group"></i>Kandidaadid<span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu" style="display: none">
										<li><a href="dashboard.php">Kuva kõik</a>
										</li>
										<li><a href="empty.html">Arhiveeritud</a>
										</li>
									</ul>
								</li>
								<li><a><i class="fa fa-edit"></i>Tegevused<span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu" style="display: none">
										<li><a href="addPerson.php">Lisa uus isik</a>
										</li>
										<li><a href="empty.html">Rollid</a>
										</li>
										<li><a href="empty.html">Etapid</a>
										</li>
										<li><a href="empty.html">Staatused</a>
										</li>									
									</ul>
								</li>
							</ul>
						</div>
					</div>
					<!-- /sidebar menu -->