<?php
	function alusta_sessioon(){
		// siin ees võiks muuta ka sessiooni kehtivusaega, aga see pole hetkel tähtis
		session_start();
	}
	
	function lopeta_sessioon(){
		$_SESSION = array();
		if (isset($_COOKIE[session_name()])) {
 			 setcookie(session_name(), '', time()-42000, '/');
		 }
		session_destroy();
	}
?>