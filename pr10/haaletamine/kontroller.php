<?php
/* Kontroller */
require_once("head.html");
include("functions.php");

alusta_sessioon();

?>
	<div id="wrap">
<?php 
$dir = "pildid";
if($_GET){
	
	$page = $_GET["page"];
	
	switch ($page) {
		case "pealeht":
			require_once("pealeht_template.html");
			break;
		case "vote":
			if(!isset($_SESSION["voted_for"])){
				require_once("vote.php");
			}else{
				require_once("voted_for_view.php");
			}
			break;
		case "galerii":
			require_once("galerii.php");
			break;
		case "tulemus":
			require_once("tulemus.php");
			break;
		case "endsession":
			lopeta_sessioon();
			header("Location: kontroller.php");
			die();
			break;
		default:
			require_once("pealeht_template.html");
	}// switch
}else{
	// parameetrit ei ole
	require_once("pealeht_template.html");
}// if			
?>	
	</div>
<?php	
	require_once("foot.html");
?>