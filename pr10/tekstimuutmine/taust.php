<?php
include("../haaletamine/functions.php");

alusta_sessioon();

$text = "";
if($_POST){
	if (in_array("", $_POST)) {
	    echo "Viga! Tühi väärtus vormil.";
	}else{
		$text = htmlspecialchars($_POST["text"]);
			$_SESSION["text"] = $text;
		$background = htmlspecialchars($_POST["background"]);
			$_SESSION["background"] = $background;
		$textcolor = htmlspecialchars($_POST["textcolor"]);
			$_SESSION["textcolor"] = $textcolor;
		$bordercolor = htmlspecialchars($_POST["bordercolor"]);
			$_SESSION["bordercolor"] = $bordercolor;
		$borderstyle = htmlspecialchars($_POST["borderstyle"]);
			$_SESSION["borderstyle"] = $borderstyle;
		$border = htmlspecialchars($_POST["border"]);
			$_SESSION["border"] = $border;
		$radius = htmlspecialchars($_POST["radius"]);
			$_SESSION["radius"] = $radius;
	}// if 
}else{
		$_SESSION["text"] = "";
		$_SESSION["background"] = "#ffffff";
		$_SESSION["textcolor"] = "#ffffff";
		$_SESSION["bordercolor"] = "#ffffff";
		$_SESSION["borderstyle"] = "none";
		$_SESSION["border"] = "1";
		$_SESSION["radius"] = "0";
}// if
$borderstyles = array("solid", "dashed", "dotted", "none");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Sessiooniga vorm valikuteks - praktikum 10</title>
	</head>
	<style>
	#tekst{
		background:<?php echo $background; ?>;
		border:<?php echo $border."px ".$borderstyle." ".$bordercolor; ?>;
		border-radius: <?php echo $radius."%"; ?>;
		color:<?php echo $textcolor; ?>;
	}
	
	</style>
<body>
	<div id="tekst">
		<p><?php echo $text; ?></p>
	</div>
	<form action="taust.php" method="POST">
		<fieldset>
			<legend><h3>Vorminda tekst</h3></legend>
			<label for="text">Tekst (sisesta tekst)</label><br>
			<textarea rows="6" cols="100" name="text"><?php echo $_SESSION["text"]; ?></textarea><br><br>
			<label for="background">Taustavärv</label>
			<input type="color" name="background" value="<?php echo $_SESSION["background"]?>">
			<label for="textcolor">Teksti värv</label>
			<input type="color" name="textcolor" value="<?php echo $_SESSION["textcolor"]?>">
			<label for="bordercolor">Piirjoone värvus</label>
			<input type="color" name="bordercolor" value="<?php echo $_SESSION["bordercolor"]?>">	<br><br>
			<label for="borderstyle">Piirjoone stiil</label>
			<select name="borderstyle">
					<?php 
							foreach($borderstyles as $style){
								$borderstyleselected = "";
								if($style == $_SESSION["borderstyle"]) $borderstyleselected = "selected";
			                   	 	echo "<option value=\"$style\" ".$borderstyleselected.">".$style."</option>";
							}
			      	?>
			</select>
			<label for="border">Piirjoone laius (1-5px)</label>
			<input type="number" name="border" min="1" max="5" value="<?php echo $_SESSION["border"]?>">
			<label for="radius">Piirjoone nurga raadius (0-50px)</label>
			<input type="number" name="radius" min="0" max="50" value="<?php echo $_SESSION["radius"]?>"><br><br>
			<input type="submit" name="Submit">
			<input type="button" onclick="location.href='end.php';" value="Puhasta väljad" />
	</fieldset>
	</form>
</body>
</html>
