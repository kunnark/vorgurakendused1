# Kunnar Kukk
# Praktikum 11

# Tabeli loomine
CREATE TABLE `vorgurakendused`.`loomaaed` ( `ID` INT NOT NULL AUTO_INCREMENT , `NIMI` VARCHAR(200) NOT NULL , `VANUS` TINYINT NOT NULL , `LIIK` VARCHAR(200) NOT NULL , `PUUR` MEDIUMINT NOT NULL , PRIMARY KEY (`ID`)) ENGINE = InnoDB COMMENT = 'Võrgurakendused praktikum 11 Loomaaed';

# Lisame 5 looma
INSERT INTO `loomaaed` (`ID`, `NIMI`, `VANUS`, `LIIK`, `PUUR`) VALUES (NULL, 'Joosep', '20', 'Elevant', '1'), (NULL, 'Antu', '12', 'Tiiger', '1'), (NULL, 'Joonas', '2', 'Elevant', '3'), (NULL, 'Marta', '4', 'Ilves', '2'), (NULL, 'Ilse', '20', 'Ilves', '2')

# Hankida kõigi mingis ühes kindlas puuris elavate loomade nimi ja puuri number
SELECT NIMI, PUUR FROM `loomaaed` WHERE PUUR = 2;

# Hankida vanima ja noorima looma vanused
# noorim:
SELECT * FROM `loomaaed` ORDER BY VANUS asc limit 0,1;
# vanim:
SELECT * FROM `loomaaed` ORDER BY VANUS desc limit 0,1;

# hankida puuri number koos selles elavate loomade arvuga (vihjeks: group by ja count )
SELECT PUUR, COUNT(PUUR) FROM `loomaaed` GROUP BY PUUR;

# suurendada kõiki tabelis olevaid vanuseid 1 aasta võrra
update loomaaed set VANUS = (SELECT VANUS)+1;