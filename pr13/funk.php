<?php

function connect_db(){
	global $connection;
	$host="localhost";
	$user="***";
	$pass="***";
	$db="***";
	$connection = mysqli_connect($host, $user, $pass, $db) or die("ei saa ühendust mootoriga- ".mysqli_error());
	mysqli_query($connection, "SET CHARACTER SET UTF8") or die("Ei saanud baasi utf-8-sse - ".mysqli_error($connection));
}

function logi(){
	// siia on vaja funktsionaalsust (13. nädalal)
	if($_SERVER['REQUEST_METHOD'] == "POST"){
		if(in_array("", $_POST)){
			$errors[] = "Viga: vähe sisendeid.";
			include_once('views/login.html');
		}else{
			$username = mysqli_real_escape_string($GLOBALS['connection'], $_POST['user']);
			$password = mysqli_real_escape_string($GLOBALS['connection'], $_POST['pass']);
			connect_db();
			
			// Küsime kasutajanime
			$query = "SELECT username, ID FROM `kukukk_kylastajad`";
			$result = mysqli_query($GLOBALS['connection'], $query) or die("$query - ".mysqli_error($GLOBALS['connection']));	
			$row = mysqli_fetch_assoc($result);
			
			if(in_array($username, $row)){
				// username ok
				$id = $row["ID"];
				$query = "SELECT passw FROM `kukukk_kylastajad` WHERE ID = ".$row["ID"];
				$result = mysqli_query($GLOBALS['connection'], $query) or die("$query - ".mysqli_error($GLOBALS['connection']));	
				$row = mysqli_fetch_assoc($result);
				
				if($row["passw"] == sha1($password)){
					// login success
					$_SESSION["user"] = $id;
					header("Location: loomaaed.php");
				}else{
					// login failed - wrong password
					$errors[] = "Vale parool või kasutajanimi";
					include_once('views/login.html');
				}// if 
			}else{
				// username does not exist
				$errors[] = "Vale parool või kasutajanimi";
				include_once('views/login.html');
			}// if in_array		
		}// if in_array
	}else{
		include_once('views/login.html');
	}// if login
}

function logout(){
	$_SESSION=array();
	session_destroy();
	header("Location: ?");
}

function kuva_puurid(){
	// siia on vaja funktsionaalsust
	if(!empty($_SESSION["user"])){
		connect_db();
		$query = "SELECT NIMI, LIIK, PUUR FROM `kukukk_loomaaed` GROUP by PUUR, ID";
	
		$result = mysqli_query($GLOBALS['connection'], $query) or die("$query - ".mysqli_error($connection));	

		$puurid = array();
	
		while($row = mysqli_fetch_assoc($result)) {
			$puurid[] = $row;
		}// while
	
		include_once("views/puurid.php");
	
	}else{
		header("Location: loomaaed.php");
	}// if 	
}


function lisa(){
	// siia on vaja funktsionaalsust (13. nädalal)
	if(!empty($_SESSION["user"])){
		if($_SERVER['REQUEST_METHOD'] == "POST"){
			// postitus on tehtud
			if(in_array("", $_POST) || $_FILES["liik"]["error"] > 0 ){
				$errors[] = "Mingi väli jäi postitamisel tühjaks või on faili üleslaadimisel tekkinud viga.";
				include_once('views/loomavorm.html');
			}else{
				// kõik ok, laeme faili üles ja teeme kirje tabelisse
				connect_db();
				upload("liik");
				$query = "INSERT INTO kukukk_loomaaed (NIMI, PUUR, LIIK) 
							VALUES ('"
								.mysqli_real_escape_string($GLOBALS['connection'], $_POST["nimi"])."', '"
								.mysqli_real_escape_string($GLOBALS['connection'], $_POST["puur"])."', '"
								."pildid/".$_FILES["liik"]["name"].
									"');";
								
				$result = mysqli_query($GLOBALS['connection'], $query) or die("$query - ".mysqli_error($GLOBALS['connection']));
				echo "Uus kirje loodud: " . mysqli_insert_id($GLOBALS['connection']) . "Värskendan lehte 3 sekundi pärast.";
				header("refresh:3; url=loomaaed.php?page=lisa");
			}// if
		}else{
			include_once('views/loomavorm.html');
		}// if $_SERVER	
	}else{
		header("Location: loomaaed.php?page=login");
	}// if !empty
}

function upload($name){
	$allowedExts = array("jpg", "jpeg", "gif", "png");
	$allowedTypes = array("image/gif", "image/jpeg", "image/png","image/pjpeg");
	$test = explode(".", $_FILES[$name]["name"]); 
	$extension = end($test);
	
	if ( in_array($_FILES[$name]["type"], $allowedTypes)
		&& ($_FILES[$name]["size"] < 100000)
		&& in_array($extension, $allowedExts)) {
    // fail õiget tüüpi ja suurusega
		if ($_FILES[$name]["error"] > 0) {
			$_SESSION['notices'][]= "Return Code: " . $_FILES[$name]["error"];
			return "";
		} else {
      // vigu ei ole
			if (file_exists("pildid/" . $_FILES[$name]["name"])) {
        // fail olemas ära uuesti lae, tagasta failinimi
				$_SESSION['notices'][]= $_FILES[$name]["name"] . " juba eksisteerib. ";
				return "pildid/" .$_FILES[$name]["name"];
			} else {
        // kõik ok, aseta pilt
				move_uploaded_file($_FILES[$name]["tmp_name"], "pildid/" . $_FILES[$name]["name"]);
				return "pildid/" .$_FILES[$name]["name"];
			}
		}
	} else {
		return "";
	}
}

?>