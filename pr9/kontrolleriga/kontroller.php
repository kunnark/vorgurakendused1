<?php
/* Kontroller */
	require_once("head.html");
?>
	<div id="wrap">
<?php 
$dir = "pildid";

if($_GET){
	
	$page = $_GET["page"];
	
	switch ($page) {
		case "pealeht":
			require_once("pealeht_template.html");
			break;
		case "vote":
			require_once("vote.php");
			break;
		case "galerii":
			require_once("galerii.php");
			break;
		case "tulemus":
			require_once("tulemus.php");
			break;
		default:
			require_once("pealeht_template.html");
	}// switch
}else{
	// parameetrit ei ole
	require_once("pealeht_template.html");
	
}// if			
?>	
	</div>
<?php	
	require_once("foot.html");
?>