<?php 
require_once("head.html");
?>
<div id="wrap">
	<h3>Valiku tulemus</h3>
<?php 
	
$dir = "pildid";
	

	if($_GET){
		
		//*******
		// Teeme spagetikoodi, aga näiteks, kuidas faili kontrollida.
		
		$failid = array(); // massiiv, kuhu lisatakse leitud failid
		if ($dh = opendir($dir)) { // kui funktsioon opendir vastava sisendiga õnnestub, siis jäta viide kaustale meelde muutujasse $dh ning läbi järgnev koodiblokk
			while (($file = readdir($dh)) !== false) { // seni, kuni funktsiooniga readdir vastavas kaustas saab kätte mingi kirje (fail/kaust), salvesta see kirje muutujasse 		$file ning läbi järgnev koodiblokk
	  		  if(!is_dir($file)) { // juhul, kui saadud kirje ei ole kaust, siis lisa antud kirje failide massiivi
	   		   $failid[] = $file;
	   	 }
	  	}
	 	 closedir($dh); // kui kausta lugemine on läbi, sulge ühendus kaustaga.
		}else{ // kui funktsioon opendir luhtub(kaust puudub), siis esita veateade ja lõpeta programmi töö
	  	  	die("Ei suuda avada kataloogi $dir");
		}
		//*******
		$pildinr = $_GET["pilt"]+1;
		
		if(in_array("nameless".$pildinr.".jpg", $failid)){
			$valik = $_GET["pilt"];
			echo "Valisid pildi nr: ".$valik;
		}else{
			echo "Faili ei leitud.";
		}// if
		
	}else{
		echo "Väärtust ei postitatud, mine ja hääleta uuesti. <br/>";
		echo "<a href=\"vote.php\">Tagasi hääletama</a>";
	}// if
?>
</div>
<?php 
	require_once("foot.html");
?>